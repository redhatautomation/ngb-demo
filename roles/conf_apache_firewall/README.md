### What this role does

This role ensures httpd and firewalld are installed and started, and allows port 80 through the firewall

### Using the role (example)

```yaml
---
- name: Configure Apache and Firewall
  hosts: all
  become: yes
  gather_facts: false

  roles:
    - role: redhatautomation.aws.conf_apache_firewall
```
